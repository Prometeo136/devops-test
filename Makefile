.PHONY: help up setup start stop test

POST_MESSAGE=................................................................
PROJECT=DEVOPS-TEST
DOCKER_COMPOSE_FILE=docker-compose-local.yaml
docker-compose:=docker-compose -f $(DOCKER_COMPOSE_FILE)

help:
	@echo 'Makefile for: ${PROJECT}'
	@echo ''
	@echo '  Options:'
	@echo '    setup             set up the project'
	@echo '    start             start the project'
	@echo '    stop              stop the container(s)'
	@echo '    restart           restart the container(s)'
	@echo '    test              run tests'
	@echo '    logs              show logs'
	@echo '    tinit             initializes the terraform environment'
	@echo '    tplan             creates execution plan'
	@echo '    tapply            execute the actions proposed in a Terraform plan'
	@echo ''


setup:
	@echo "Building image from docker compose ${POST_MESSAGE}"
	$(docker-compose) build --no-cache

start:
	@echo "Starting the container ${POST_MESSAGE}"
	$(docker-compose) up -d

stop:
	@echo "Stopping the container ${POST_MESSAGE}"
	$(docker-compose) stop

restart:
	@make -s stop
	@make -s start

test:
	@echo "Running tests ${POST_MESSAGE}"
	#???????????

logs:
	$(docker-compose) logs -f

tinit:
	terraform init

tplan:
	terraform plan

tapply:
	terraform apply
