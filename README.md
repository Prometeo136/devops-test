# DevOps Challenge

Para trabajar con este proyecto se necesita lo siguiente:
* Tener instalado docker y docker-compose.
* El usuario actual debe pertenecer al grupo docker.
* Tener un usuario en AWS con permisos suficientes para crear una instancia EC2.
* Tener configuradas las credenciales en ~/.aws/credentials.

El poyecto tiene un archivo Makefile, el cual contiene comandos comunes para hacer más fácil el desarrollo y despliegue del proyecto.
Solo basta con ejecutar **make help** para listar las opciones disponibles y qué hace cada una.
Si se quiere ejecutar alguno de los comandos en el archivo Makefile solo basta con ejecutar en la consola make «comando»
