variable "ami" {
    type = string
    default = "ami-0cff7528ff583bf9a"
}

variable "instance_type" {
    type = string
    default = "t2.micro"
}

variable "volume_size" {
  type = numeric
  default = 20
}

variable "volume_type" {
  type = string
  default = "gp2"
}

variable "allowed_host" {
  type = string
  default = "11.22.33.44"
}
